# curl -X POST -H "Cache-Control: no-cache" -H "Content-Type: application/zip" --data-binary @myPackage.zip "http://localhost:9423/service/rest/convert.pdf" > result.pdf
require 'rubygems'
require "base64"

require File.expand_path('../../lib/ZipFileGenerator.rb',  __FILE__)

directory_to_zip = "../input"
output_file = "../tmp/out.zip"
zf = ZipFileGenerator.new(directory_to_zip, output_file)
zf.write()
content =

content = Base64.encode64(IO.read(output_file))


require File.expand_path('../../lib/PDFreactor.rb',  __FILE__)

pdfReactor = PDFreactor.new("http://localhost:8888/service/rest")

# Create a new PDFreactor configuration object
config = {
    # Specify the input document
    document: content,
    logLevel: PDFreactor::LogLevel::WARN,
    # Set the title of the created PDF
    title: "Demonstration of the PDFreactor Ruby API",
    # Set the author of the created PDF
    author: "Myself",
}
connectionSettings = {}
connectionSettings[:headers] =  {"content-type": "application/zip"}



begin
    # Convert document
    documentId = pdfReactor.convertAsync(config, connectionSettings);
    puts "documentId: #{documentId}"

    loop do
        progress = pdfReactor.getProgress(documentId)
        break if progress["finished"]
        sleep(0.5)
    end

    print "Content-type: application/pdf\n\n"
    # Streaming is more efficient for larger documents
    $stdout.binmode
    pdfReactor.getDocumentAsBinary(documentId, $stdout);
rescue PDFreactor::PDFreactorWebserviceError => error
    print "Content-type: text/html\n\n"
    puts "<h1>An Error Has Occurred</h1>"
    puts "<h2>#{error.message}</h2>"
end
